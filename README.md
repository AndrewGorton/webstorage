# Html5OfflineStorage

Test project for HTML 5 offline storage.

## Compile
  `./sbt assembly`

## Run
  `java -jar target/WebStorage-assembly-1.0.0-SNAPSHOT.jar server config.json`

  Also works with Heroku ("stage" aliased to "assembly", and the Procfile is set up to work - `git push heroku develop:master` is the command you want).

## Test

Go to http://localhost:8080

## Development Environment

  * MacOS => 10.9.2
  * `java -version` => `Java 1.7.0_45`
