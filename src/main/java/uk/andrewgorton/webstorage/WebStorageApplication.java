package uk.andrewgorton.webstorage;

import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import uk.andrewgorton.webstorage.configuration.WebStorageConfiguration;
import uk.andrewgorton.webstorage.resources.WebStorageResource;

public class WebStorageApplication extends Application<WebStorageConfiguration> {
    public static void main(String[] args) throws Exception {
        new WebStorageApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<WebStorageConfiguration> bootstrap) {
        //bootstrap.addBundle(new ViewBundle());
        bootstrap.addBundle(new AssetsBundle("/assets/", "/assets"));
    }

    @Override
    public void run(WebStorageConfiguration configuration,
                    Environment environment) {
        final WebStorageResource resource = new WebStorageResource();
        environment.jersey().register(resource);
    }
}
