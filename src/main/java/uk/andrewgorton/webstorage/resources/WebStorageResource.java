package uk.andrewgorton.webstorage.resources;

import com.codahale.metrics.annotation.Timed;
import org.joda.time.DateTime;
import uk.andrewgorton.webstorage.interfaces.*;

import javax.imageio.ImageIO;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class WebStorageResource {
    private static Parcels parcels;
    private static Covers covers;
    private static Uses uses;
    private static final String cacheTag = String.format("%d", System.currentTimeMillis());

    private static String lockToken;

    static {
        List<String> tempCovers = new ArrayList<String>();
        tempCovers.add("Arable");
        tempCovers.add("Permanent Pasture");
        covers = new Covers();
        covers.setCovers(tempCovers);

        List<String> tempUses = new ArrayList<String>();
        tempUses.add("Wheat");
        tempUses.add("Barley");
        tempUses.add("Grassland");
        uses = new Uses();
        uses.setUses(tempUses);


        List<Parcel> tempParcels = new ArrayList<Parcel>();
        for (int i = 0; i < 10; i++) {
            Parcel tempParcel = new Parcel();
            tempParcel.setId(String.format("SJ1234 %04d", i));
            tempParcel.setSize(String.format("%d", i + 1));
            tempParcel.setDateOfChange(DateTime.now().toString());

            tempParcel.setCover(covers.getCovers().get(i % covers.getCovers().size()));
            tempParcel.setUse(uses.getUses().get(i % uses.getUses().size()));


            tempParcel.setArea(String.format("%d", (i + 1) * 10));


            tempParcel.setNewCover(tempParcel.getCover());
            tempParcel.setNewUse(tempParcel.getUse());

            tempParcel.setIncludedInBpsClaim((i % 2 == 0 ? true : false));
            tempParcel.setNewIncludedInBpsClaim(tempParcel.isIncludedInBpsClaim());

            tempParcels.add(tempParcel);
        }
        parcels = new Parcels();
        parcels.setParcels(tempParcels);
    }

    public WebStorageResource() {

    }

    @GET
    @Timed
    public void redirectToStaticHtml() {
        try {
            URI uri = new URI("/assets/index.html");
            Response response = Response.seeOther(uri).build();
            throw new WebApplicationException(response);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @GET
    @Timed
    @Path("/rest/parcels")
    public Parcels getParcels() {
        return parcels;
    }

    @GET
    @Timed
    @Path("/rest/covers")
    public Covers getCovers() {
        return covers;
    }


    @GET
    @Timed
    @Path("/rest/uses")
    public Uses getUses() {
        return uses;
    }

    @PUT
    @Timed
    @Path("/rest/parcels")
    public Result setParcels(Parcels parcels) {
        this.parcels = parcels;

        // Accept all the offline changes
        for (Parcel singleParcel : this.parcels.getParcels()) {
            if (isParcelPendingChanges(singleParcel)) {
                singleParcel.setCover(singleParcel.getNewCover());
                singleParcel.setUse(singleParcel.getNewUse());
                singleParcel.setIncludedInBpsClaim(singleParcel.isNewIncludedInBpsClaim());
                singleParcel.setDateOfChange(DateTime.now().toString());
            }
        }

        Result r = new Result();
        r.setSuccess(true);
        return r;
    }

    private boolean isParcelPendingChanges(final Parcel parcel) {
        if (parcel.getCover().compareToIgnoreCase(parcel.getNewCover()) != 0) {
            return true;
        }

        if (parcel.getUse().compareToIgnoreCase(parcel.getNewUse()) != 0) {
            return true;
        }

        if (parcel.isIncludedInBpsClaim() != parcel.isNewIncludedInBpsClaim()) {
            return true;
        }

        return false;
    }

    @GET
    @Timed
    @Path("/rest/jpg")
    @Produces("image/jpg")
    public Response getImage() {

        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            ImageIO.write(generate(), "jpg", out);

            final byte[] imgData = out.toByteArray();

            final InputStream bigInputStream =
                    new ByteArrayInputStream(imgData);

            return Response.ok(bigInputStream).build();
        } catch (final IOException e) {
            return Response.noContent().build();
        }
    }

    private BufferedImage generate() {
        int width = 800;
        int height = 800;
        int step = 10;

        BufferedImage bi = new BufferedImage(width, height,
                BufferedImage.TYPE_3BYTE_BGR);
        Graphics2D g = (Graphics2D) bi.getGraphics();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, width, height);


        g.setColor(Color.BLACK);
        for (int counter = 0; counter < width / 2; counter += step) {
            Ellipse2D.Double hole = new Ellipse2D.Double();
            hole.width = width - (counter * step);
            hole.height = height - (counter * step);
            hole.x = counter * step;
            hole.y = counter * step;
            g.draw(hole);
        }

        g.dispose();
        return bi;
    }

    @GET
    @Path("/cache.manifest")
    @Produces("text/cache-manifest")
    public String getManifest() {
        StringBuilder sb = new StringBuilder();
        sb.append("CACHE MANIFEST\n");
        sb.append("# ");
        sb.append(cacheTag);
        sb.append("\n");

        //sb.append("/rest/covers\n");
        //sb.append("/rest/uses\n");

        sb.append("CACHE:\n");
        sb.append("/assets/app.js\n");
        sb.append("/assets/angular.min.js\n");
        sb.append("/assets/index.html\n");
        sb.append("/assets/main.css\n");
        sb.append("/assets/maincontroller.js\n");

        sb.append("NETWORK:\n");
        sb.append("*\n");

        return sb.toString();
    }
}
