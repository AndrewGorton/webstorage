package uk.andrewgorton.webstorage.interfaces;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Result {
    private Boolean success;
    private String message;

    public Result() {

    }

    @JsonProperty
    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    @JsonProperty
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
