package uk.andrewgorton.webstorage.interfaces;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Covers {
    private List<String> covers;

    public Covers() {

    }

    @JsonProperty
    public List<String> getCovers() {
        return covers;
    }

    public void setCovers(List<String> covers) {
        this.covers = covers;
    }
}
