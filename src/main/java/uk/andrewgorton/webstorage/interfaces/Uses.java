package uk.andrewgorton.webstorage.interfaces;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Uses {
    private List<String> uses;

    public Uses() {

    }

    @JsonProperty
    public List<String> getUses() {
        return uses;
    }

    public void setUses(List<String> uses) {
        this.uses = uses;
    }
}
