package uk.andrewgorton.webstorage.interfaces;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Parcel {
    private String id;
    private String size;
    private String dateOfChange;

    private String cover;
    private String use;
    private String area;
    private boolean includedInBpsClaim;

    private String newCover;
    private String newUse;
    private boolean newIncludedInBpsClaim;

    public Parcel() {

    }

    @JsonProperty
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty
    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    @JsonProperty
    public String getDateOfChange() {
        return dateOfChange;
    }

    public void setDateOfChange(String dateOfChange) {
        this.dateOfChange = dateOfChange;
    }

    @JsonProperty
    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    @JsonProperty
    public String getUse() {
        return use;
    }

    public void setUse(String use) {
        this.use = use;
    }

    @JsonProperty
    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    @JsonProperty
    public String getNewCover() {
        return newCover;
    }

    public void setNewCover(String newCover) {
        this.newCover = newCover;
    }

    @JsonProperty
    public String getNewUse() {
        return newUse;
    }

    public void setNewUse(String newUse) {
        this.newUse = newUse;
    }

    @JsonProperty
    public boolean isIncludedInBpsClaim() {
        return includedInBpsClaim;
    }

    public void setIncludedInBpsClaim(boolean includedInBpsClaim) {
        this.includedInBpsClaim = includedInBpsClaim;
    }

    @JsonProperty
    public boolean isNewIncludedInBpsClaim() {
        return newIncludedInBpsClaim;
    }

    public void setNewIncludedInBpsClaim(boolean newIncludedInBpsClaim) {
        this.newIncludedInBpsClaim = newIncludedInBpsClaim;
    }

}
