package uk.andrewgorton.webstorage.interfaces;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Parcels {
    public List<Parcel> parcels;

    @JsonProperty
    public List<Parcel> getParcels() {
        return parcels;
    }

    public void setParcels(List<Parcel> parcels) {
        this.parcels = parcels;
    }

}
