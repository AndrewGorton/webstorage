function MainController($scope, $http) {
    $scope.deleteOfflineData = function() {
        localStorage.removeItem("storedParcels")
        localStorage.removeItem("storedCovers")
        localStorage.removeItem("storedUses")
    }

    $scope.getOfflineData = function() {
        $scope.parcels = JSON.parse(localStorage.getItem("storedParcels"));
        $scope.covers = JSON.parse(localStorage.getItem("storedCovers"));
        $scope.uses = JSON.parse(localStorage.getItem("storedUses"));
    }

    $scope.saveOfflineData = function() {
        localStorage.setItem("storedParcels", JSON.stringify($scope.parcels));
        localStorage.setItem("storedCovers", JSON.stringify($scope.covers));
        localStorage.setItem("storedUses", JSON.stringify($scope.uses));
    }

    $scope.logEvent = function(event) {
        console.log(event);
    }

     $scope.init = function() {
        if (window.applicationCache) {
             //window.applicationCache.addEventListener('checking',$scope.logEvent,false);
             window.applicationCache.addEventListener('noupdate',$scope.logEvent,false);
             window.applicationCache.addEventListener('downloading',$scope.logEvent,false);
             window.applicationCache.addEventListener('cached',$scope.logEvent,false);
             window.applicationCache.addEventListener('updateready',$scope.logEvent,false);
             window.applicationCache.addEventListener('obsolete',$scope.logEvent,false);
             window.applicationCache.addEventListener('error',$scope.logEvent,false);
         }
         else
         {
             console.log("null window.applicationCache");
         }

        if(localStorage.getItem("storedParcels") === null) {
            $scope.offlineMode = false;

            $http({ method: 'GET', url: '/rest/parcels' }).
                success(function (data, status, headers, config) {
                    $scope.parcels = data;
                }).
                error(function (data, status, headers, config) {
                    throw "Failed to download parcels";
                });

            $http({ method: 'GET', url: '/rest/covers' }).
                success(function (data, status, headers, config) {
                    $scope.covers = data;
                }).
                error(function (data, status, headers, config) {
                    throw "Failed to download covers";
                });

            $http({ method: 'GET', url: '/rest/uses' }).
                success(function (data, status, headers, config) {
                    $scope.uses = data;
                }).
                error(function (data, status, headers, config) {
                    throw "Failed to download uses";
                });
          }
          else
          {
                $scope.offlineMode = true;
                $scope.getOfflineData();
          }
     }

    $scope.onClickWipeOfflineBtn = function() {
        $scope.deleteOfflineData();

        location.reload();
    }

    $scope.onClickTakeOfflineBtn = function() {
        $scope.saveOfflineData();

        location.reload();
    }

    $scope.onClickSaveBtn = function() {
        $http({ method: 'PUT', url: '/rest/parcels', data: $scope.parcels }).
                success(function (data, status, headers, config) {
                    if(status == 200) {
                       location.reload();
                    }
                }).
                error(function (data, status, headers, config) {
                    throw "Failed to save";
                });
    }

    $scope.onClickResetBtn = function() {
        var parcelCount = $scope.parcels.parcels.length;
        for(var i = 0; i < parcelCount; ++i) {
            $scope.parcels.parcels[i].newCover = $scope.parcels.parcels[i].cover;
            $scope.parcels.parcels[i].newUse = $scope.parcels.parcels[i].use;
        }
     }

     $scope.isParcelChanged = function(parcel) {
        if(parcel.cover === parcel.newCover
            && parcel.use === parcel.newUse
            && parcel.includedInBpsClaim == parcel.newIncludedInBpsClaim) {
            return true;
        }
        return false;
     }

    $scope.onClickUploadAndGoOnlineBtn = function() {
        $http({ method: 'PUT', url: '/rest/parcels', data: $scope.parcels }).
            success(function (data, status, headers, config) {
                if(status == 200) {
                   $scope.deleteOfflineData();
                   location.reload();
                }
            }).
            error(function (data, status, headers, config) {
                throw "Failed to upload";
            });
    }

    $scope.onClickWipeLocalStorageBtn = function() {
        $scope.deleteOfflineData();
        location.reload();
    }

    $scope.init();
};

app.controller('MainController', ['$scope', '$http', MainController]);