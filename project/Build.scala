import sbt._
import Keys._
import sbtassembly.Plugin._

object BuildSettings {
  val buildOrganization = "uk.andrewgorton"
  val buildVersion      = "1.0.0-SNAPSHOT"
  val buildScalaVersion = "2.11.1"

  val buildSettings = Seq (
    organization := buildOrganization,
    version      := buildVersion,
    scalaVersion := buildScalaVersion,
    crossPaths := false
  )
}

object Dependencies {
  val dropwizardCore = "io.dropwizard" % "dropwizard-core" % "0.7.0"
  val dropwizardAssets = "io.dropwizard" % "dropwizard-assets" % "0.7.0"
}

object WebStorageBuild extends Build {
  import Dependencies._
  import BuildSettings._

  val serviceDependancies = Seq (
    dropwizardCore,
    dropwizardAssets
  )

  lazy val webStorage = Project (
    "WebStorage",
    file ("."),
    settings = buildSettings
      ++ assemblySettings
      ++ addCommandAlias("stage", ";assembly")
      ++ Seq (libraryDependencies ++= serviceDependancies)
      ++ com.ebiznext.sbt.plugins.SonarPlugin.sonar.settings
  )

}
